#!/bin/bash

u="u"

echo "downloading service zip"
for (( i=0; i<5; i++ )); do
    if curl -L -o /home/$u/services.zip https://bitbucket.org/jayzranga/m/get/develop.zip && unzip -t "/home/$u/services.zip"; then
        break
    fi
    echo "downloaded failed $((i+1)) time(s)"
    sleep 5
done

if ! unzip -t "/home/$u/services.zip"; then
    echo "failed to download service files"
    exit 1
fi

rm -rf /home/$u/umx/package/ && mkdir -p /home/$u/umx/package/

echo "installing service"
unzip /home/$u/services.zip -d /home/$u/umx/package/
shopt -s dotglob
mv /home/$u/umx/package/*/* /home/$u/umx/package/

bash /home/$u/umx/package/services/s/ct-start.sh