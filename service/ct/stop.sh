#!/bin/bash

# stop xorg service
systemctl stop m.service

# stop gpu configuration service
systemctl stop g.service