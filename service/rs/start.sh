#!/bin/bash

chmod 400 /home/u/umx/package/.auth/rs-id
rt=$(echo $HOSTNAME | sed -n "s/^\([^0-9]*\).*$/\1/p")
rn=$(echo $HOSTNAME | sed -n "s/^[^0-9]*\([0-9]*\)$/\1/p")
sp=$(expr $(printf '%d\n' "'$rt") \* 100 + $rn)
mp=$(expr $(printf '%d\n' "'$rt") \* 300 + $rn)

# unit set to burst at 10 failures within 5 minutes with check interval of 20 seconds
sleep 10
autossh -M $mp -o StrictHostKeyChecking=no -i /home/u/umx/package/.auth/rs-id -N -R $sp:localhost:22 ubuntu@54.193.133.63