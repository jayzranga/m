#!/usr/bin/expect

set u "u"
set pn [lindex $argv 0];
set pw [lindex $argv 1];

spawn  ssh -o StrictHostKeyChecking=no $u@localhost -p $pn
expect -gl "*localhost's password:*"
send "$pw\n";
expect -gl "*$u@*"
send -- "sudo -s\n"
expect "*password for $u:*"
send "$pw\n"
expect -gl "*root@*"
send "journalctl -fu m.service\n"
interact