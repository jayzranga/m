using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace watchdog
{
    public interface IDog
    {
        Task Watch(CancellationToken stoppingToken);
    }

    public class Gpu
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Vendor { get; set; }
        public string BusId { get; set; }
        public string NvId { get; set; }
        public int Power { get; set; }
        public int Temp { get; set; }
        public int Fan { get; set; }
        public double Hashrate { get; set; }
    }

    public class Dog : IDog
    {
        private readonly ILogger<Dog> _logger;
        private readonly IEmailSender _emailSender;

        public Dog(ILogger<Dog> logger, IEmailSender emailSender)
        {
            _logger = logger;
            _emailSender = emailSender;
        }

        public async Task Watch(CancellationToken stoppingToken)
        {
            _logger.LogInformation("dog starting watch...");
            var errors = 0;
            var idles = 0;
            var worker = string.Empty;
            var temp = -1;
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromSeconds(30));
                if (errors >= 4)
                {
                    var msg = $"{worker} restarting! due to {errors} errors";
                    _logger.LogWarning(msg);
                    await _emailSender.Send(msg);
                    Restart();
                    continue;
                }
                if (idles >= 4)
                {
                    var msg = $"{worker} restarting! due to {idles} idles";
                    _logger.LogWarning(msg);
                    await _emailSender.Send(msg);
                    Restart();
                    continue;
                }

                var client = new HttpClient { Timeout = TimeSpan.FromSeconds(10) };
                HttpResponseMessage response = null;
                try
                {
                    response = await client.GetAsync("http://localhost:4067/summary");
                }
                catch (Exception e)
                {
                    var msg = $"{worker} miner stopped! error {++errors}";
                    _logger.LogWarning(e, msg);
                    await _emailSender.Send(msg);
                    continue;
                }

                if (errors > 0)
                {
                    var msg = $"{worker} miner recovered! after {errors} errors";
                    _logger.LogInformation(msg);
                    await _emailSender.Send(msg);
                }
                errors = 0;

                var content = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                var hashrate = (double?)content["hashrate"] / (1000 * 1000) ?? -1;
                worker = (string)content["active_pool"]["worker"];
                _logger.LogInformation($"{response.StatusCode} {DateTime.Now.ToLocalTime():yyyy-MM-dd HH:mm:ss} {hashrate:F} MH/s");
                
                if (hashrate <= 0 || content["gpus"] == null || content["gpus"].Count() == 0)
                {
                    var msg = $"{worker} miner stopped! idle {++idles}";
                    _logger.LogWarning(msg);
                    await _emailSender.Send(msg);
                    continue;
                }

                if (idles > 0)
                {
                    var msg = $"{worker} miner recovered! after {idles} idles";
                    _logger.LogInformation(msg);
                    await _emailSender.Send(msg);
                }
                idles = 0;

                var gpus = content["gpus"]
                            .Select(g => new Gpu { Id = (string)g["gpu_id"], Name = (string)g["name"], Vendor = (string)g["vendor"], BusId = (string)g["pci_bus"], Fan = (int?)g["fan_speed"] ?? -1, Power = (int?)g["power"] ?? -1, Temp = (int?)g["temperature"] ?? -1, Hashrate = (double?)g["hashrate"] ?? -1}).ToList();
                foreach (var (g, i) in gpus.OrderBy(g => int.Parse(g.BusId)).Select((g, i) => (g, i))) g.NvId = $"{i}";
                var len = gpus.Max(g => $"{g.Vendor} {g.Name}".Length);
                foreach (var g in gpus)
                    _logger.LogInformation($"   GPU [M:{g.Id}, N:{g.NvId}, B:{g.BusId.PadLeft(2, '0')}]: {(g.Vendor + ' ' + g.Name).PadRight(len)} - {(g.Hashrate / (1000 * 1000)).ToString("F").PadLeft(6)} MH/s, [T:{g.Temp}C, P:{g.Power}W, F:{g.Fan}%]");
                
                // is temp is higher? send warning email at every 20th iteration
                temp = ++temp % 20;
                if (temp == 0 && gpus.Any(g => g.Temp >= 70))
                {
                    var msg = gpus.Where(g => g.Temp >= 70).Aggregate(Environment.NewLine, (g1, g2) => $"GPU [M:{g2.Id}, N:{g2.NvId}, B:{g2.BusId.PadLeft(2, '0')}]: {(g2.Vendor + ' ' + g2.Name).PadRight(len)} - {(g2.Hashrate / (1000 * 1000)).ToString("F").PadLeft(6)} MH/s, [T:{g2.Temp}C, P:{g2.Power}W, F:{g2.Fan}%]");
                    await _emailSender.Send($"{worker} temp alert!", msg);
                }
            }
            _logger.LogInformation("dog ended watch");
        }

        void Restart()
        {
            ExecuteScript("./s/restart.sh");
        }

        void ExecuteScript(string file)
        {
            var process = new Process() { StartInfo = new ProcessStartInfo() { FileName = "/bin/bash", Arguments = file, } };
            _logger.LogInformation("requesting reboot");
            process.Start();
            //process.WaitForExit();
        }
    }
}