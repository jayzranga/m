using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

public interface IEmailSender
{
    Task Send(string subject, string body = null);
}

public class EmailSender : IEmailSender
{
    private readonly ILogger<EmailSender> _logger;

    public EmailSender(ILogger<EmailSender> logger)
    {
        _logger = logger;
    }

    public async Task Send(string subject, string body = null)
    {
        var message = new MailMessage(new MailAddress("support@clovetech.com.au"), new MailAddress("support@clovetech.com.au")) { Subject = subject, Body = body};
        try
        {
            await new SmtpClient
            {
                Host = "email-smtp.us-east-2.amazonaws.com",
                Port = 587,
                Credentials = new NetworkCredential("AKIAULU47HWMXVCCUOOO", "BKHr17jbA6INDhSAbBE/YP3TLMvH6ij7N5g0LCfcIf2I"),
                EnableSsl = true,
            }.SendMailAsync(message);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "an error occurred sending email");
        }
    }
}