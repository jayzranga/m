using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace watchdog
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IDog _dog;

        public Worker(ILogger<Worker> logger, IDog dog)
        {
            _logger = logger;
            _dog = dog;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                await _dog.Watch(stoppingToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "dog hit a snag!");
            }
        }
    }
}
