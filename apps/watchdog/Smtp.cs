public class Smtp
{
    public string Server { get; set; }
    public int Port { get; set; }
    public bool Tls { get; set; }
    public string User { get; set; }
    public string Pass { get; set; }

}