﻿using System;
using System.Drawing;

namespace dash.Shared
{
    public class Pool
    {
        public string Name { get; set; }
        public long Balance { get; set; }
        public Worker[] Workers { get; set; }
        public Uri Endpoint { get; set; }
        public Uri Dashboard { get; set; }
        public Address[] Addresses {get;set;}
    }

    public class Worker
    {
        public string Name { get; set; }

        public double ReportedHashrate { get; set; }

        public double CurrentHashrate { get; set; }

        public DateTimeOffset LastSeen { get; set; }

        public int ValidShares { get; set; }

        public int InvalidShares { get; set; }

        public int StaleShares { get; set; }
    }

    public class Address
    {
        public string Id {get;set;}
        public string Name {get;set;}
        public Color Color {get;set;}
        public string Endpoint => $"https://etherchain.org/account/{Id}";
        public Balance Balance {get;set;}
    }

    public class Balance
    {
        public decimal Eth {get;set;}
    }
}