﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace dash.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RateController : ControllerBase
    {
        private readonly ILogger<RateController> _logger;

        public RateController(ILogger<RateController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<decimal> Get()
        {
            return (decimal)JsonConvert.DeserializeObject<JObject>(await (await new HttpClient { Timeout = TimeSpan.FromSeconds(10) }
                .GetAsync("https://query1.finance.yahoo.com/v8/finance/chart/ETH-AUD?region=US&lang=en-US&includePrePost=false&interval=2m&useYfid=true&range=1d&corsDomain=finance.yahoo.com&.tsrc=finance"))
                .Content.ReadAsStringAsync())["chart"]["result"][0]["meta"]["regularMarketPrice"];
        }
    }
}