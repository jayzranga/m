﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using dash.Shared;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using AngleSharp;
using System.Text.RegularExpressions;
using System.Drawing;

namespace dash.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DashboardController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private static Address[] Addresses = new []
        {
           new Address { Id = "0xe4b545DEDBE90d62205E69F4E982572d00E74345", Name = "A", Color = Color.Red},
           new Address { Id = "0x7E4b498606e57491b5ABEFa71255Fd6c140A87Ea", Name = "B", Color = Color.Yellow},
           new Address { Id = "0x7c3679d1DC1AD2417B7F68b1d837A46301C0534b", Name = "C", Color = Color.Green},
           new Address { Id = "0x4B3356A0F95e181d2407F2d041855303Db04De7F", Name = "X", Color = Color.Gray}
        };

        private readonly ILogger<DashboardController> _logger;

        public DashboardController(ILogger<DashboardController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IEnumerable<Pool>> Get()
        {
            return await Task.WhenAll(Addresses.Select(async a =>
                await GetPool(new Pool {
                    Name = $"Etherpool {a.Name}",
                    Dashboard = new Uri($"https://ethermine.org/miners/{a.Id}/dashboard"),
                    Endpoint = new Uri($"https://api.ethermine.org/miner/{a.Id}/dashboard"),
                    Addresses = new [] { await Balance(a) }
                })
            ));
        }

        private async Task<Address> Balance(Address address)
        {
            address.Balance = new Balance
            {
                Eth = decimal.Parse(Regex.Match((await BrowsingContext.New(Configuration.Default.WithDefaultLoader())
                        .OpenAsync(address.Endpoint)).QuerySelector(".tab-content .text-monospace .badge-success").TextContent, @"(\d+(\.\d*))").Groups[1].Value)
            };
            return address;
        }

        // [HttpGet]
        // public async Task<Balance> GetBalance()
        // {
        //     return new Balance { Eth = (await Task.WhenAll(Addresses.Select(a => Balance(new Address { Endpoint = $"https://etherchain.org/account/{a}" })))).Sum(a => a.Balance) };
        // }

        private async Task<Pool> GetPool(Pool pool)
        {
            var client = new HttpClient { Timeout = TimeSpan.FromSeconds(10) };
            var response = await client.GetAsync(pool.Endpoint);
            var content = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
            var workers = content["data"]["workers"].Select(w => new Worker
            {
                Name = (string)w["worker"],
                CurrentHashrate = (double)w["currentHashrate"],
                ReportedHashrate = (double)w["reportedHashrate"],
                LastSeen = DateTimeOffset.FromUnixTimeSeconds((long)w["lastSeen"])
            });
            pool.Balance = (long?)content["data"]["currentStatistics"]["unpaid"] ?? 0;
            pool.Workers = workers.ToArray();
            return pool;
        }
    }
}
