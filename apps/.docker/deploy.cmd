az login
az acr login --name mxdash

docker context use default

docker-compose -f .\docker-compose-dash-acr.yml build
docker-compose -f .\docker-compose-dash-acr.yml push

export u=ubuntu
rm /home/$u/services.zip
curl -L -o /home/$u/services.zip https://bitbucket.org/jayzranga/m/get/develop.zip
rm -rf /home/$u/umx/package/ && mkdir -p /home/$u/umx/package/
unzip /home/$u/services.zip -d /home/$u/umx/package/
shopt -s dotglob
mv /home/$u/umx/package/*/* /home/$u/umx/package/
find /home/$u/umx/package/service/ -type f -name "*.service" -exec chmod 444 {} \;
find /home/$u/umx/package/service/ -type f -name "*.sh" -exec chmod 111 {} \;
systemctl daemon-reload
systemctl restart da.service
journalctl -fu da.service