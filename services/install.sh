#!/bin/bash

u="u"

apt update
apt -y install curl
curl -L -o /home/$u/services.zip https://bitbucket.org/jayzranga/m/get/develop.zip

rm -rf /home/$u/umx/package/ && mkdir -p /home/$u/umx/package/
unzip /home/$u/services.zip -d /home/$u/umx/package/
shopt -s dotglob
mv /home/$u/umx/package/*/* /home/$u/umx/package/

mkdir -p /home/$u/umx/service/
cp -r /home/$u/umx/package/service/ct/ /home/$u/umx/service/

echo "set permssions on services"
find /home/$u/umx/service/ -type f -name "*.service" -exec chmod 444 {} \;
find /home/$u/umx/service/ -type f -name "*.sh" -exec chmod 111 {} \;

echo "register services"
find /home/$u/umx/service/ -type f -name "*.service" -exec cp {} /lib/systemd/system \;

echo "enable service catalyst"
systemctl enable ct.service

bash /home/$u/umx/package/services/s/ct-start.sh
