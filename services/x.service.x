[Unit]
Description=x service
Wants=network-online.target
After=network.target

[Install]
WantedBy=default.target

[Service]
ExecStart=/home/u/umx/package/services/s/x-start.sh
TimeoutStopSec=60
SyslogIdentifier=xorgkill