#!/bin/bash

u="u"
# set path to util scripts
export PATH=$PATH:/home/$u/umx/package/services/u

echo "set permssions on services"
find /home/$u/umx/package/services/ -type f -name "*.service" -exec chmod 444 {} \;
find /home/$u/umx/package/services/ -type f -name "*.sh" -exec chmod 111 {} \;
find /home/$u/umx/package/service/ -type f -name "*.service" -exec chmod 444 {} \;
find /home/$u/umx/package/service/ -type f -name "*.sh" -exec chmod 111 {} \;

# install dependencies
bash /home/$u/umx/package/services/s/dep.sh

echo "disable services"
find /home/$u/umx/package/services/ -type f -name "*.service" -printf "%f\n" | xargs systemctl disable;
find /home/$u/umx/package/service/ ! -name ct.service -type f -name "*.service" -printf "%f\n" | xargs systemctl disable;

# echo "fixup windows style endings on services and files"
# sed -i 's/\r//g' /home/$u/umx/package/services/*.service
# sed -i 's/\r//g' /home/$u/umx/package/services/*/*.sh

echo "register services"
find /home/$u/umx/package/services/ -type f -name "*.service" -exec cp -t /lib/systemd/system {} \;
find /home/$u/umx/package/service/ ! -name ct.service -type f -name "*.service" -exec cp -t /lib/systemd/system {} \;
systemctl daemon-reload

echo "setting up miners"
mkdir -p /home/$u/umx/package/miners/

### TREX ###
tver="0.24.8"
if ! tar tf "/home/$u/t-rex-$tver-linux.tar.gz"; then
    echo "downloading trex miner"
    for (( i=0; i<5; i++ )); do
        if curl -L -o /home/$u/t-rex-$tver-linux.tar.gz https://github.com/trexminer/T-Rex/releases/download/$tver/t-rex-$tver-linux.tar.gz && tar tf "/home/$u/t-rex-$tver-linux.tar.gz"; then
            break
        fi
        echo "downloaded failed $((i+1)) time(s)"
        sleep 5
    done
fi
if ! tar tf "/home/$u/t-rex-$tver-linux.tar.gz"; then
    echo "failed to download trex miner"
    exit 1
fi

echo "installing trex miner"
rm -rf /home/$u/umx/package/miners/t-rex-$tver-linux/ && mkdir -p /home/$u/umx/package/miners/t-rex-$tver-linux/
tar -xvf /home/$u/t-rex-$tver-linux.tar.gz -C /home/$u/umx/package/miners/t-rex-$tver-linux/
### TREX ###

if grep 'GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"' /etc/default/grub; then
    sed -i 's/^GRUB_CMDLINE_LINUX_DEFAULT=.*$/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash reboot=warm,cold,bios,smp,triple,kbd,acpi,efi,pci,force"/g' /etc/default/grub
    update-grub
    echo "setup complete. rebooting now..."
    reboot now
fi

if ! nvidia-smi; then
    apt -y --purge remove nvidia-*
    apt -y autoremove
    apt -y install nvidia-driver-470
fi

# echo "start services"
find /home/$u/umx/package/services/ -type f -name "*.service" -printf "%f\n" | xargs systemctl restart;
find /home/$u/umx/package/service/ ! -name ct.service -type f -name "*.service" -printf "%f\n" | xargs systemctl restart;

echo "setup complete"