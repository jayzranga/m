#!/bin/bash

#cd /home/u/umx/package/apps/.docker/ && docker-compose -f docker-compose-watchdog.yml up --build

echo "compiling watchdog"
cd /home/u/umx/package/apps/watchdog/
while (! dotnet publish -c Release -o ./publish/); do
    sleep 10
    #dotnet restore
    #dotnet build -c Release -o ./build/ && 
done
cd publish && dotnet watchdog.dll