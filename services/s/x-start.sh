#!/bin/bash

# wait a bit before killing main display
sleep 50
#nvidia-smi -pm 0
# kill main display
while (ps -C Xorg -o pid=); do
    kill -15 $(ps -C Xorg -o pid=)
    sleep 5
done
#nvidia-smi -pm 1

# start new xorg
X :101