#!/bin/bash

# enable persistence mode
nvidia-smi -pm 1
# set lower power limit
nvidia-smi -pl 140
# # start with low power limits
# for (( i=0; i<$(nvidia-smi --query-gpu=name --format=csv,noheader | wc -l); i++ ))
# do
#         nvidia-smi -i $i -pl 120
# done

wallet="0xe4b545DEDBE90d62205E69F4E982572d00E74345"

# apply machine specific config
. /home/u/umx/package/services/g/$HOSTNAME.sh

mkdir -p /home/u/logs/
log_date=`date +"%Y-%m-%d"`

# start trex miner
tver="0.24.8"
/home/u/umx/package/miners/t-rex-$tver-linux/t-rex -a ethash -o stratum+ssl://eu1.ethermine.org:5555 -u $wallet -p x -w $HOSTNAME --fan t:60 --temperature-limit 75 --temperature-start 60 --watchdog-exit-mode 5:5:r -l /home/u/logs/trex-log-$log_date.txt $trex
#/home/u/umx/package/miners/PhoenixMiner_5.6d_Linux/PhoenixMiner -pool eu1.ethermine.org:4444 -wal 0x1a8b5463Ac42bbF2DffEB4758C06678114B05D4A.$HOSTNAME -tt -60 -tstop 75 -tstart 60 -fanmin 60 -coin eth -logfile phoenix-log-*.txt -logdir /home/u/logs/