#!/bin/bash

# enable overclocking
nvidia-xconfig -a --allow-empty-initial-configuration --cool-bits=28 --enable-all-gpus
# enable persistence mode
nvidia-smi -pm 1
# set lower power limit
nvidia-smi -pl 140

X :1 &

# apply gpu configuration. try multiple times
while true; do
    sleep 10
    export DISPLAY=:1
    if nvidia-settings -a [gpu]/GPUFanControlState=1 -a [fan]/GPUTargetFanSpeed=80 && sh /home/u/umx/package/services/g/$HOSTNAME.sh; then
        break
    fi
done