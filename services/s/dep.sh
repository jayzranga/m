echo "installing dependencies"
apt -y install curl
apt -y install openssh-server
apt -y install autossh ssh

wget https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
apt update
apt install -y apt-transport-https
apt update
apt install -y dotnet-sdk-5.0
apt install -y net-tools
apt install -y expect
apt install -y nmap

# docker not required for now
#apt -y install docker-compose