# hashrate: 400 -> 385?
# [M:0, N:0, B:05]: EVGA NVIDIA RTX 3080
# [M:1, N:1, B:06]: EVGA NVIDIA RTX 3080
# [M:2, N:2, B:07]: EVGA NVIDIA RTX 3080
# [M:3, N:3, B:10]: EVGA NVIDIA RTX 3080

wallet="0x7E4b498606e57491b5ABEFa71255Fd6c140A87Ea"
trex="--lock-cclock 1060"

nvidia-smi -pl 240
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1800
# # cuda exception
# nvidia-settings -a [gpu:1]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1500
# nvidia-settings -a [gpu:2]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1500

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90