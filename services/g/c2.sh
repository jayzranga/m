# hashrate: 340 -> 336
# [M:0, N:4, B:41]: NVIDIA NVIDIA RTX 3080
# [M:1, N:0, B:33]: MSI NVIDIA RTX 3070
# [M:2, N:1, B:37]: EVGA NVIDIA RTX 3070
# [M:3, N:2, B:38]: EVGA NVIDIA RTX 3070
# [M:4, N:3, B:39]: EVGA NVIDIA RTX 3070

wallet="0x7E4b498606e57491b5ABEFa71255Fd6c140A87Ea"
trex="--lock-cclock 1060,885,885,885,885"

nvidia-smi -pl 140
nvidia-smi -i 4 -pl 240
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2200
# # overrides #
# # reason: hashrate drops to 93 on NVIDIA NVIDIA RTX 3080. for some reason its at 0 though it says 4 above
# nvidia-settings -a [gpu:0]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2000
# nvidia-settings -a [fan:0]/GPUTargetFanSpeed=80
# nvidia-settings -a [fan:1]/GPUTargetFanSpeed=80

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90