# hashrate: 320 -> 322
# [M:0, N:0, B:33]: EVGA NVIDIA RTX 3080
# [M:1, N:1, B:38]: EVGA NVIDIA RTX 3080
# [M:2, N:2, B:39]: MSI NVIDIA RTX 3070
# [M:3, N:3, B:40]: MSI NVIDIA RTX 3070

wallet="0x7c3679d1DC1AD2417B7F68b1d837A46301C0534b"
trex="--lock-cclock 1060,1060,885,885"

nvidia-smi -i 0 -pl 240
nvidia-smi -i 1 -pl 340

nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2350

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90