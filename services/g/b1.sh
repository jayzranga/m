# hashrate: 400 -> 388?
# [M:0, N:0, B:04]: EVGA NVIDIA RTX 3080
# [M:1, N:1, B:05]: EVGA NVIDIA RTX 3080
# [M:2, N:2, B:06]: EVGA NVIDIA RTX 3080
# [M:3, N:3, B:07]: EVGA NVIDIA RTX 3080

wallet="0x7c3679d1DC1AD2417B7F68b1d837A46301C0534b"
trex="--lock-cclock 1300,885,1300,885,1300,1300,885"

# nvidia-smi -pl 240
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2400
# # overrides #
# # hash drops to low 90's
# nvidia-settings -a [gpu:1]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
# nvidia-settings -a [fan:2]/GPUTargetFanSpeed=80
# nvidia-settings -a [fan:3]/GPUTargetFanSpeed=80

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2000
# nvidia-settings -a [gpu:4]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90