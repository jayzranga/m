# hashrate: 720 -> 708
# [M:0, N:0, B:04]: MSI NVIDIA RTX 3090
# [M:1, N:1, B:05]: MSI NVIDIA RTX 3090
# [M:2, N:2, B:06]: MSI NVIDIA RTX 3090
# [M:3, N:3, B:07]: MSI NVIDIA RTX 3090
# [M:4, N:4, B:12]: MSI NVIDIA RTX 3090
# [M:5, N:5, B:14]: MSI NVIDIA RTX 3090

trex="--lock-cclock 1060"

nvidia-smi -pl 340
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2200
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90