# hashrate: 800 -> 773
# [M:0, N:0, B:04]: Gainward NVIDIA RTX 3080
# [M:1, N:1, B:05]: Gainward NVIDIA RTX 3080
# [M:2, N:2, B:06]: Gainward NVIDIA RTX 3080
# [M:3, N:3, B:07]: Gainward NVIDIA RTX 3080
# [M:4, N:4, B:12]: Gainward NVIDIA RTX 3080
# [M:5, N:5, B:13]: Gainward NVIDIA RTX 3080
# [M:6, N:6, B:14]: Gainward NVIDIA RTX 3080
# [M:7, N:7, B:15]: Gainward NVIDIA RTX 3080

trex="--lock-cclock 1060"

nvidia-smi -pl 240
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1700
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90