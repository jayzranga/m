# hashrate: 560 -> ?
# [M:0, N:0, B:04]: NVIDIA RTX 3080
# [M:1, N:1, B:05]: NVIDIA RTX 3080
# [M:2, N:2, B:06]: NVIDIA RTX 3080
# [M:3, N:3, B:07]: NVIDIA RTX 3070
# [M:4, N:4, B:13]: NVIDIA RTX 3080
# [M:5, N:5, B:15]: NVIDIA RTX 3080

wallet="0x7E4b498606e57491b5ABEFa71255Fd6c140A87Ea"
trex="--lock-cclock 1060,1060,1060,885,1060,1060"

nvidia-smi -i 0 -pl 240
nvidia-smi -i 1 -pl 240
nvidia-smi -i 2 -pl 240
nvidia-smi -i 4 -pl 240
nvidia-smi -i 5 -pl 240

nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2000
# nvidia-settings -a [fan]/GPUTargetFanSpeed=80
# # overrides #
# # reason: cuda exception
# nvidia-settings -a [gpu:0]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1900
# nvidia-settings -a [gpu:1]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1900
# nvidia-settings -a [gpu:5]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1900

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90