# hashrate: 600 -> 579
# GPU [M:0, N:0, B:04]: Gigabyte NVIDIA RTX 3080
# GPU [M:1, N:1, B:05]: Gigabyte NVIDIA RTX 3080
# GPU [M:2, N:2, B:06]: ASUS NVIDIA RTX 3080
# GPU [M:3, N:3, B:07]: Gainward NVIDIA RTX 3080
# GPU [M:4, N:4, B:13]: EVGA NVIDIA RTX 3080
# GPU [M:5, N:5, B:15]: EVGA NVIDIA RTX 3080

trex="--lock-cclock 1060"

nvidia-smi -pl 240
# msi gaming z trio bios updated with asus strix
nvidia-smi -i 2 -pl 340
# msi gaming x trio bios updated with evga ftw3
nvidia-smi -i 4 -pl 340
# msi gaming z trio bios updated with evga ftw3
nvidia-smi -i 5 -pl 340

nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2200
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90
# # overrides #
# # reason: thermal throttles
# nvidia-settings -a [gpu:0]/GPUMemoryTransferRateOffsetAllPerformanceLevels=400
# # reason: thermal throttles
# nvidia-settings -a [gpu:1]/GPUMemoryTransferRateOffsetAllPerformanceLevels=300
# # reason: noticed 1 restart in m.service logs
# nvidia-settings -a [gpu:3]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2100
# # reason: cuda exception at 3080 msi dressed as evga ftw3
# nvidia-settings -a [gpu:4]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1300
# # reason: cuda exception at 3080 msi dressed as evga ftw3
# nvidia-settings -a [gpu:5]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1600


# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [gpu:0]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
nvidia-settings -a [gpu:1]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
nvidia-settings -a [gpu:4]/GPUMemoryTransferRateOffsetAllPerformanceLevels=800
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90