# hashrate: 250 -> 251
# [M:0, N:0, B:38]: EVGA NVIDIA RTX 3080
# [M:1, N:1, B:39]: EVGA NVIDIA RTX 3070
# [M:2, N:2, B:40]: EVGA NVIDIA RTX 3070
# [M:3, N:3, B:41]: MSI NVIDIA GTX 1660 Ti

wallet="0x7E4b498606e57491b5ABEFa71255Fd6c140A87Ea"
trex="--lock-cclock 1060,885,885,1000"

nvidia-smi -pl 140
nvidia-smi -i 0 -pl 240
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2200
# # overrides #
# # reason: goes idle at times
# nvidia-settings -a [gpu:0]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2000
# nvidia-settings -a [fan:0]/GPUTargetFanSpeed=80
# nvidia-settings -a [fan:1]/GPUTargetFanSpeed=80

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90