# hashrate: 500 -> 492
# [M:0, N:0, B:04]: EVGA NVIDIA RTX 3080
# [M:1, N:1, B:05]: EVGA NVIDIA RTX 3080
# [M:2, N:2, B:06]: EVGA NVIDIA RTX 3080
# [M:3, N:3, B:07]: Gainward NVIDIA RTX 3080
# [M:4, N:4, B:10]: Gigabyte NVIDIA RTX 3080

trex="--lock-cclock 1060"

nvidia-smi -pl 240
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2200
# nvidia-settings -a [fan]/GPUTargetFanSpeed=80
# # overrides #
# # reason: ?, changing gpu:0 below changes gpu:4 at the rig
# # reason: ?, changing gpu:4 below changes gpu:0 at the rig
# nvidia-settings -a [gpu:4]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1900
# nvidia-settings -a [gpu:3]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1900

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90