# hashrate: 400 -> 401
# [M:0, N:0, B:04]: EVGA NVIDIA RTX 3080
# [M:1, N:1, B:06]: EVGA NVIDIA RTX 3080
# [M:2, N:2, B:07]: EVGA NVIDIA RTX 3080
# [M:3, N:3, B:10]: EVGA NVIDIA RTX 3080

# K's rig
wallet="0xe8E22cE86E511ed083f8876c9701BA9126328077"
# wallet="0x237369F38A54A57C04816a689aA8E81613811d4A"
trex="--lock-cclock 1060"

nvidia-smi -pl 240
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2400

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90