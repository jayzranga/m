# hashrate: 280 -> 277
# [M:0, N:2, B:06]: EVGA NVIDIA RTX 3080
# [M:1, N:0, B:04]: EVGA NVIDIA RTX 3070
# [M:2, N:1, B:05]: MSI NVIDIA RTX 3070
# [M:3, N:3, B:07]: EVGA NVIDIA RTX 3060 Ti

trex="--lock-cclock 1060,885,885,1300"

nvidia-smi -pl 140
nvidia-smi -i 2 -pl 240
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2300
# # overrides #
# # reason: going idle
# nvidia-settings -a [gpu:0]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1700
# # reason: going idle
# nvidia-settings -a [gpu:3]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1700

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90