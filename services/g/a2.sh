# hashrate: 300 -> 307
# [M:0, N:0, B:37]: MSI NVIDIA RTX 3070
# [M:1, N:1, B:38]: MSI NVIDIA RTX 3070
# [M:2, N:2, B:39]: MSI NVIDIA RTX 3070
# [M:3, N:3, B:40]: MSI NVIDIA RTX 3070
# [M:4, N:4, B:41]: MSI NVIDIA RTX 3070

wallet="0x7c3679d1DC1AD2417B7F68b1d837A46301C0534b"
trex="--lock-cclock 885"

nvidia-smi -pl 140
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2400

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90