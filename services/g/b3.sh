# hashrate: 400 -> 380?
# [M:0, N:0, B:05]: EVGA NVIDIA RTX 3080
# [M:1, N:1, B:06]: EVGA NVIDIA RTX 3080
# [M:2, N:2, B:07]: EVGA NVIDIA RTX 3080
# [M:3, N:3, B:10]: EVGA NVIDIA RTX 3080

wallet="0x7c3679d1DC1AD2417B7F68b1d837A46301C0534b"
trex="--lock-cclock 1060,1060,1060,1060,1060,1060,1060,885"

nvidia-smi -pl 140
nvidia-smi -i 1 -pl 240
nvidia-smi -i 2 -pl 240
nvidia-smi -i 3 -pl 240
nvidia-smi -i 4 -pl 240
nvidia-smi -i 5 -pl 240
nvidia-smi -i 6 -pl 240
nvidia-smi -i 7 -pl 240

nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2000
# # overrides #
# # ?
# nvidia-settings -a [fan]/GPUTargetFanSpeed=80
# # gpu 1, bus 6 is falling off
# nvidia-settings -a [gpu:1]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# # gpu 2 and 3 fails with cuda exceptions
# nvidia-settings -a [gpu:2]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1800
# nvidia-settings -a [gpu:3]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1800

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90