wallet="0x7c3679d1DC1AD2417B7F68b1d837A46301C0534b"
trex="--lock-cclock 1300"

nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2000

nvidia-settings -a [fan]/GPUTargetFanSpeed=90