# hashrate: 800 -> 794
# [M:0, N:0, B:04]: EVGA RTX 3080
# [M:1, N:1, B:05]: EVGA RTX 3080
# [M:2, N:2, B:06]: EVGA RTX 3080
# [M:3, N:3, B:07]: EVGA RTX 3080
# [M:4, N:4, B:12]: EVGA RTX 3080
# [M:5, N:5, B:13]: EVGA RTX 3080
# [M:6, N:6, B:14]: EVGA RTX 3080
# [M:7, N:7, B:15]: EVGA RTX 3080

wallet="0x7E4b498606e57491b5ABEFa71255Fd6c140A87Ea"
trex="--lock-cclock 1060"

nvidia-smi -pl 240
nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2100
# nvidia-settings -a [fan]/GPUTargetFanSpeed=80

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90