# hashrate: 380 -> 356
# [M:0, N:2, B:39]: MSI NVIDIA RTX 3080
# [M:1, N:4, B:41]: Colorful NVIDIA RTX 3080
# [M:2, N:0, B:33]: Gigabyte NVIDIA RTX 3070
# [M:3, N:1, B:38]: Gigabyte NVIDIA RTX 3070
# [M:4, N:3, B:40]: Gigabyte NVIDIA RTX 3070

wallet="0x7E4b498606e57491b5ABEFa71255Fd6c140A87Ea"
trex="--lock-cclock 885,885,1060,885,1060"

nvidia-smi -pl 140
nvidia-smi -i 2 -pl 240
nvidia-smi -i 4 -pl 240

nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200

# # setting: normal
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2000
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90
# # overrides #
# nvidia-settings -a [gpu:0]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1600
# nvidia-settings -a [gpu:2]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
# nvidia-settings -a [gpu:4]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000

# setting: light
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1000
nvidia-settings -a [fan]/GPUTargetFanSpeed=90

# # setting: lazy
# nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=0
# nvidia-settings -a [fan]/GPUTargetFanSpeed=90