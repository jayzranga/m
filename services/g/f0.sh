trex="--lock-cclock 1300"

nvidia-settings -a [gpu]/GPUGraphicsClockOffsetAllPerformanceLevels=-200
nvidia-settings -a [gpu]/GPUMemoryTransferRateOffsetAllPerformanceLevels=2000

nvidia-settings -a [fan]/GPUTargetFanSpeed=90