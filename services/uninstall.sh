#!/bin/bash

u="u"

find /home/$u/sandbox/services/ -type f -name "*.service" -printf "%f\n" | xargs systemctl disable
find /home/$u/sandbox/services/ -type f -name "*.service" -printf "%f\n" | xargs systemctl stop

find /home/$u/umx/package/services/ -type f -name "*.service" -printf "%f\n" | xargs systemctl disable
find /home/$u/umx/package/services/ -type f -name "*.service" -printf "%f\n" | xargs systemctl stop
find /home/$u/umx/package/service/ -type f -name "*.service" -printf "%f\n" | xargs systemctl disable
find /home/$u/umx/package/service/ -type f -name "*.service" -printf "%f\n" | xargs systemctl stop

systemctl disable c.service
systemctl stop c.service

rm -rf /home/$u/sandbox/
rm -rf /home/$u/umx/